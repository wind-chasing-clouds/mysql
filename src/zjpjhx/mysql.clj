(ns zjpjhx.mysql
  (:use korma.core)
  (:require [korma.db :only (defdb)]))


(comment def db-spec
  {:subprotocol "mysql"
   :subname "//localhost:3306/chihuo"
   :user "oscar"
   :password "alice"
   :useUnicode "yes"
   :characterEncoding "UTF-8"
   :delimiters "`"
   :useLegacyDatetimeCode "false"
   :serverTimezone "GMT"})

;(defentity users)
;(korma.db/defdb db db-spec)

(defn- get-fields-from-table [table]
  (map :Field (exec-raw [ (str "SHOW columns FROM `" table "`") ] :results)))

(def get-fields (memoize get-fields-from-table))


(defn find-in-set
  "check col is have value set , return 0 if not , else return 1"
  [col value]
   (sqlfn "FIND_IN_SET" value (raw (name col))))

(defn sql-set
  "设置 mysql set 字段的值, 在原值上加一个值,n 根据set字段定义要加第n个值,n从0开始计.
  用法: (update table (set-fields (sql-set set-field 1)))
        as-sql: udpate table set set-field = set-field | 2 "
  [col n]
    {(keyword col)  (raw  (str (name col) "|" (bit-set 0 n)))})

(defmacro find-table [table & sqlwhere]
  `(->
    (select* ~table)
    ~@sqlwhere
    (select)))

(defn select-table [table id]
  (-> table (select*) (where {:id id}) (select) (first)))

(defn insert-table [table datas]
  (let [fields (get-fields table)]
    (-> table (insert*)
      (values (map #(select-keys % fields) datas))
      (insert)
      (:GENERATED_KEY))))

(defn delete-table [table datas]
  "删除表记录,对于通讯录,物品类等,不删除,只设删除标志"
  (let [ids (map #(:id %)  datas)]
    (if (nil? (#{"通讯录" "物品类"} table))
      (delete table
              (where {:id [in ids]}))
      (update table
              (set-fields (sql-set :状态 0))
              (where {:id [in ids]})))))

(defn update-table [table datas]
  (let [fields (get-fields table)
         rows (map  #(select-keys % fields) datas)]
     (doseq [row rows]
       (update table
               (set-fields row)
               (where {:id (:id row)})))))
